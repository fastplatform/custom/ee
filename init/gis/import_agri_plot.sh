# Region: ee/ESTONIA
# Object: agri_plot
# Download URL: https://kls.pria.ee/geoserver/pria_avalik/ows?typename=pria_avalik:pria_pollud
# S3 URL: https://oss.eu-west-0.prod-cloud-ocb.orange-business.com/fastplatform-tmp-public/ee/gis/agri_plot/PRIA_pollud.zip
#
# This import script expects the following variables to be set:
# -------------------------------------------------------------
# VERSION_ID: ID of the version (in the target <object>_version table) that will be created
# POSTGRES_EXTERNAL_CONNECTION_STRING: connection string to the Postres external database
#                                      "PG:host='<HOST>' port='<PORT>' user='<USER>' password='<PASSWORD>' dbname='external'""
# SOURCE_PATH: URL to the zip file containing the shapefile(s)
# SOURCE_LAYER_NAME: layer name within the shapefile
# GEOMETRY_FIELD: geometry field name (usually "GEOMETRY" for shapefiles)

set -e

err_report() {
    echo "Failed on line $1"
}

trap 'err_report $LINENO' ERR

VERSION_ID="${VERSION_ID:-1000}"
POSTGRES_EXTERNAL_CONNECTION_STRING="${POSTGRES_EXTERNAL_CONNECTION_STRING:-"PG:host='localhost.fastplatform.eu' port='5438' user='fast' password='fast' dbname='external'"}"  # pragma: allowlist secret
SOURCE_PATH_LOCAL="$(pwd)/agri_plot/PRIA_pollud.zip"
SOURCE_LAYER_NAME=${SOURCE_LAYER_NAME:-"PRIA_pollud"}
SOURCE_GEOMETRY_FIELD=${SOURCE_GEOMETRY_FIELD:-"GEOMETRY"}

start=$(date +%s)

echo "[*] Delete version $VERSION_ID if it already exists and (re)create it..."
ogrinfo -q "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    -sql "
        DELETE
            FROM public.agri_plot_version
            WHERE id=$VERSION_ID;
        INSERT
            INTO public.agri_plot_version(id, is_active, import_started_at)
            VALUES ($VERSION_ID, FALSE, '$(date +"%Y-%m-%dT%H:%M:%S%z")')
        "

echo "[*] Load from distant source..."
# Filter only agricultural LPIS codes for Bulgaria, see NTP.xlsx
PGCLIENTENCODING=UTF8 ogr2ogr --debug ON \
    -f "PostgreSQL" \
    -nln "public.agri_plot" \
    -nlt "MULTIPOLYGON" \
    -gt 50000 \
    -dim 2 \
    -append -update -progress \
    -t_srs "EPSG:4258" \
    -dialect "SQLITE" \
    -sql "
        SELECT
            $VERSION_ID AS version_id,
            pollu_id AS authority_id,
            CAST(pindala_ha AS FLOAT) * 10000 AS area,
            viimase_mu AS valid_from,
            NULL AS valid_to,
            NULL AS plant_species_id,
            NULL AS plant_variety_id,
            NULL as source,
            ST_Simplify(ST_MakeValid($SOURCE_GEOMETRY_FIELD), 0) AS geometry
        FROM
            '$SOURCE_LAYER_NAME'
        WHERE
            $SOURCE_GEOMETRY_FIELD IS NOT NULL
    " \
    "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    $([ $SOURCE_PATH ] && echo "/vsizip//vsicurl/$SOURCE_PATH" || echo "/vsizip/$SOURCE_PATH_LOCAL")

echo "[*] Set version $VERSION_ID completion time..."
ogrinfo -q "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    -sql "
        UPDATE
            public.agri_plot_version
        SET
            import_completed_at='$(date +"%Y-%m-%dT%H:%M:%S%z")',
            status='COMPLETED'
        WHERE
            id=$VERSION_ID
        "

end=$(date +%s)

echo "[!] Done in $((end-start)) seconds"
