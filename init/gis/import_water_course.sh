# Region: ee/ESTONIA
# Object: water_course
# Download URL: https://geoportaal.maaamet.ee/index.php?lang_id=2&page_id=618
# S3 URL: https://oss.eu-west-0.prod-cloud-ocb.orange-business.com/fastplatform-tmp-public/ee/gis/water_course/ETAK_Eesti_SHP_veekogud.zip/E_203_vooluveekogu_a.shp
# S3 URL: https://oss.eu-west-0.prod-cloud-ocb.orange-business.com/fastplatform-tmp-public/ee/gis/water_course/ETAK_Eesti_SHP_veekogud.zip/E_203_vooluveekogu_j.shp
#
# This import script expects the following variables to be set:
# -------------------------------------------------------------
# VERSION_ID: ID of the version (in the target <object>_version table) that will be created
# POSTGRES_EXTERNAL_CONNECTION_STRING: connection string to the Postres external database
#                                      "PG:host='<HOST>' port='<PORT>' user='<USER>' password='<PASSWORD>' dbname='external'""
# SOURCE_PATH: URL to the zip file containing the shapefile(s)
# SOURCE_LAYER_NAME: layer name within the shapefile
# GEOMETRY_FIELD: geometry field name (usually "GEOMETRY" for shapefiles)

set -e

err_report() {
    echo "Failed on line $1"
}

trap 'err_report $LINENO' ERR

VERSION_ID="${VERSION_ID:-1000}"
POSTGRES_EXTERNAL_CONNECTION_STRING="${POSTGRES_EXTERNAL_CONNECTION_STRING:-"PG:host='localhost.fastplatform.eu' port='5438' user='fast' password='fast' dbname='external'"}"  # pragma: allowlist secret
SOURCE_PATH_LOCAL_VOOLUVEEKOGU_A="$(pwd)/water_course/ETAK_Eesti_SHP_veekogud.zip/E_203_vooluveekogu_a.shp"
SOURCE_LAYER_NAME_VOOLUVEEKOGU_A=${SOURCE_LAYER_NAME_VOOLUVEEKOGU_A:-"E_203_vooluveekogu_a"}
SOURCE_PATH_LOCAL_VOOLUVEEKOGU_J="$(pwd)/water_course/ETAK_Eesti_SHP_veekogud.zip/E_203_vooluveekogu_j.shp"
SOURCE_LAYER_NAME_VOOLUVEEKOGU_J=${SOURCE_LAYER_NAME__VOOLUVEEKOGU_J:-"E_203_vooluveekogu_j"}
SOURCE_GEOMETRY_FIELD=${SOURCE_GEOMETRY_FIELD:-"GEOMETRY"}

start=$(date +%s)

echo "[*] Delete version $VERSION_ID if it already exists and (re)create it..."
ogrinfo -q "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    -sql "
        DELETE
            FROM public.water_course_version
            WHERE id=$VERSION_ID;
        INSERT
            INTO public.water_course_version(id, is_active, import_started_at)
            VALUES ($VERSION_ID, FALSE, '$(date +"%Y-%m-%dT%H:%M:%S%z")')
        "

echo "[*] Load 'vooluveekogu_a' objects from distant source..."
PGCLIENTENCODING=UTF8 ogr2ogr --debug ON \
    -f "PostgreSQL" \
    -nln "public.water_course" \
    -append -update -progress \
    -gt 50000 \
    -dim 2 \
    -t_srs "EPSG:4258" \
    -dialect "SQLITE" \
    -sql "
        SELECT
            $VERSION_ID AS version_id,
            'vooluveekogu_a_' || etak_id AS authority_id,
            'vooluveekogu_a_' || etak_id AS hydro_id,
            nimetus AS geographical_name,
            muutmisaeg AS begin_lifespan_version,
            valjavote AS end_lifespan_version,
            NULL AS buffer,
            'Vooluveekogu' AS local_type,
            NULL as source,
            ST_Simplify(ST_MakeValid($SOURCE_GEOMETRY_FIELD), 0) AS geometry
        FROM
            '$SOURCE_LAYER_NAME_VOOLUVEEKOGU_A'
        WHERE
            $SOURCE_GEOMETRY_FIELD IS NOT NULL
    " \
    "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    $([ $SOURCE_PATH_VOOLUVEEKOGU_A ] && echo "/vsizip//vsicurl/$SOURCE_PATH_VOOLUVEEKOGU_A" || echo "/vsizip/$SOURCE_PATH_LOCAL_VOOLUVEEKOGU_A")

echo "[*] Load 'vooluveekogu_j' objects from distant source..."
PGCLIENTENCODING=UTF8 ogr2ogr --debug ON \
    -f "PostgreSQL" \
    -nln "public.water_course" \
    -append -update -progress \
    -gt 50000 \
    -dim 2 \
    -t_srs "EPSG:4258" \
    -dialect "SQLITE" \
    -sql "
        SELECT
            $VERSION_ID AS version_id,
            'vooluveekogu_a_' || etak_id AS authority_id,
            'vooluveekogu_a_' || etak_id AS hydro_id,
            nimetus AS geographical_name,
            muutmisaeg AS begin_lifespan_version,
            valjavote AS end_lifespan_version,
            NULL AS buffer,
            tyyp_t AS local_type,
            NULL as source,
            ST_Simplify(ST_MakeValid($SOURCE_GEOMETRY_FIELD), 0) AS geometry
        FROM
            '$SOURCE_LAYER_NAME_VOOLUVEEKOGU_J'
        WHERE
            $SOURCE_GEOMETRY_FIELD IS NOT NULL

    " \
    "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    $([ $SOURCE_PATH_VOOLUVEEKOGU_J ] && echo "/vsizip//vsicurl/$SOURCE_PATH_VOOLUVEEKOGU_J" || echo "/vsizip/$SOURCE_PATH_LOCAL_VOOLUVEEKOGU_J")

echo "[*] Set version $VERSION_ID completion time..."
ogrinfo -q "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    -sql "
        UPDATE
            public.water_course_version
        SET
            import_completed_at='$(date +"%Y-%m-%dT%H:%M:%S%z")',
            status='COMPLETED'
        WHERE
            id=$VERSION_ID
        "

end=$(date +%s)

echo "[!] Done in $((end-start)) seconds"