# Region: ee/ESTONIA
# Object: management_restriction_or_regulation_zone
# Download URL: 
# S3 URL: https://oss.eu-west-0.prod-cloud-ocb.orange-business.com/fastplatform-tmp-public/ee/gis/management_restriction_or_regulation_zone/kkr_nitraaditundlik_ala.zip
#
# This import script expects the following variables to be set:
# -------------------------------------------------------------
# VERSION_ID: ID of the version (in the target OBJECT_version table) that will be created
# POSTGRES_EXTERNAL_CONNECTION_STRING: connection string to the Postres external database
#                                      "PG:host='<HOST>' port='<PORT>' user='<USER>' password='<PASSWORD>' dbname='external'""
# SOURCE_PATH: URL to the zip file containing the shapefile(s)
# SOURCE_LAYER_NAME: layer name within the shapefile
# GEOMETRY_FIELD: geometry field name (usually "GEOMETRY" for shapefiles)

set -e

err_report() {
    echo "[-] Failed on line $1"
}

trap 'err_report $LINENO' ERR

VERSION_ID="${VERSION_ID:-1000}"
POSTGRES_EXTERNAL_CONNECTION_STRING="${POSTGRES_EXTERNAL_CONNECTION_STRING:-"PG:host='localhost.fastplatform.eu' port='5438' user='fast' password='fast' dbname='external'"}"  # pragma: allowlist secret
SOURCE_PATH_LOCAL="$(pwd)/management_restriction_or_regulation_zone/kkr_nitraaditundlik_ala.zip"
SOURCE_LAYER_NAME=${SOURCE_LAYER_NAME:-"kkr_nitraaditundlik_ala"}
SOURCE_GEOMETRY_FIELD=${SOURCE_GEOMETRY_FIELD:-"GEOMETRY"}

start=$(date +%s)

echo "[*] Delete version $VERSION_ID if it already exists and (re)create it..."
ogrinfo -q "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    -sql "
        DELETE
            FROM public.management_restriction_or_regulation_zone_version
            WHERE id=$VERSION_ID;
        INSERT
            INTO public.management_restriction_or_regulation_zone_version(id, is_active, import_started_at)
            VALUES ($VERSION_ID, FALSE, '$(date +"%Y-%m-%dT%H:%M:%S%z")')
        "

echo "[*] Load from distant source..."
PGCLIENTENCODING=UTF8 ogr2ogr --debug ON \
    -f "PostgreSQL" \
    -nln "public.management_restriction_or_regulation_zone" \
    -nlt "MULTIPOLYGON" \
    -append -update -progress \
    -gt 50000 \
    -dim 2 \
    -t_srs "EPSG:4258" \
    -dialect "SQLITE" \
    -sql "
        SELECT
            $VERSION_ID AS version_id,
            kr_kood AS authority_id,
            NULL AS thematic_id,
            nimi AS name,
            NULL AS designation_period,
            NULL AS begin_lifespan_version,
            NULL AS end_lifespan_version,
            NULL AS specialized_zone_type_id,
            NULL AS source,
            ST_Simplify(ST_MakeValid($SOURCE_GEOMETRY_FIELD), 0) AS geometry
        FROM
            '$SOURCE_LAYER_NAME'
        WHERE
            $SOURCE_GEOMETRY_FIELD IS NOT NULL
    " \
    "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    $([ $SOURCE_PATH ] && echo "/vsizip//vsicurl/$SOURCE_PATH" || echo "/vsizip/$SOURCE_PATH_LOCAL")

echo "[*] Assign NVZ zone type codes..."
ogrinfo -q "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    -sql "
    INSERT INTO public.management_restriction_or_regulation_zone_zone_type(managementrestrictionorregulationzone_id, zonetypecode_id) 
            SELECT 
                management_restriction_or_regulation_zone.id AS managementrestrictionorregulationzone_id,
                'http://inspire.ec.europa.eu/codelist/ZoneTypeCode/nitrateVulnerableZone' AS designationtype_id 
            FROM public.management_restriction_or_regulation_zone
            WHERE management_restriction_or_regulation_zone.version_id=$VERSION_ID;
    "

echo "[*] Set version $VERSION_ID completion time..."
ogrinfo -q "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    -sql "
        UPDATE
            public.management_restriction_or_regulation_zone_version
        SET
            import_completed_at='$(date +"%Y-%m-%dT%H:%M:%S%z")',
            status='COMPLETED'
        WHERE
            id=$VERSION_ID
        "

end=$(date +%s)

echo "[!] Done in $((end-start)) seconds"
