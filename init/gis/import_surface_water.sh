# Region: ee/ESTONIA
# Object: surface_water
# Download URL: https://geoportaal.maaamet.ee/index.php?lang_id=2&page_id=618
# S3 URL:
# - https://oss.eu-west-0.prod-cloud-ocb.orange-business.com/fastplatform-tmp-public/ee/gis/surface_water/ETAK_Eesti_SHP_veekogud.zip/E_201_meri_a.shp
# - https://oss.eu-west-0.prod-cloud-ocb.orange-business.com/fastplatform-tmp-public/ee/gis/surface_water/ETAK_Eesti_SHP_veekogud.zip/E_202_seisuveekogu_a.shp
# - https://oss.eu-west-0.prod-cloud-ocb.orange-business.com/fastplatform-tmp-public/ee/gis/surface_water/ETAK_Eesti_SHP_veekogud.zip/E_202_seisuveekogu_p.shp
#
# This import script expects the following variables to be set:
# -------------------------------------------------------------
# VERSION_ID: ID of the version (in the target <object>_version table) that will be created
# POSTGRES_EXTERNAL_CONNECTION_STRING: connection string to the Postres external database
#                                      "PG:host='<HOST>' port='<PORT>' user='<USER>' password='<PASSWORD>' dbname='external'""
# SOURCE_PATH: URL to the zip file containing the shapefile(s)
# SOURCE_LAYER_NAME: layer name within the shapefile
# GEOMETRY_FIELD: geometry field name (usually "GEOMETRY" for shapefiles)

set -e

err_report() {
    echo "Failed on line $1"
}

trap 'err_report $LINENO' ERR

VERSION_ID="${VERSION_ID:-1000}"
POSTGRES_EXTERNAL_CONNECTION_STRING="${POSTGRES_EXTERNAL_CONNECTION_STRING:-"PG:host='localhost.fastplatform.eu' port='5438' user='fast' password='fast' dbname='external'"}"  # pragma: allowlist secret
SOURCE_PATH_LOCAL_MERI="$(pwd)/surface_water/ETAK_Eesti_SHP_veekogud.zip/E_201_meri_a.shp"
SOURCE_LAYER_NAME_MERI=${SOURCE_LAYER_NAME_MERI:-"E_201_meri_a"}
SOURCE_PATH_LOCAL_SEISUVEEKOGU_A="$(pwd)/surface_water/ETAK_Eesti_SHP_veekogud.zip/E_202_seisuveekogu_a.shp"
SOURCE_LAYER_NAME_SEISUVEEKOGU_A=${SOURCE_LAYER_NAME_SEISUVEEKOGU_A:-"E_202_seisuveekogu_a"}
SOURCE_PATH_LOCAL_SEISUVEEKOGU_P="$(pwd)/surface_water/ETAK_Eesti_SHP_veekogud.zip/E_202_seisuveekogu_p.shp"
SOURCE_LAYER_NAME_SEISUVEEKOGU_P=${SOURCE_LAYER_NAME_SEISUVEEKOGU_P:-"E_202_seisuveekogu_p"}
SOURCE_GEOMETRY_FIELD=${SOURCE_GEOMETRY_FIELD:-"GEOMETRY"}

start=$(date +%s)

echo "[*] Delete version $VERSION_ID if it already exists and (re)create it..."
ogrinfo -q "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    -sql "
        DELETE
            FROM public.surface_water_version
            WHERE id=$VERSION_ID;
        INSERT
            INTO public.surface_water_version(id, is_active, import_started_at)
            VALUES ($VERSION_ID, FALSE, '$(date +"%Y-%m-%dT%H:%M:%S%z")')
        "

echo "[*] Load 'meri' objects from distant source..."
PGCLIENTENCODING=UTF8 ogr2ogr --debug ON \
    -f "PostgreSQL" \
    -nln "public.surface_water" \
    -append -update -progress \
    -gt 50000 \
    -dim 2 \
    -t_srs "EPSG:4258" \
    -dialect "SQLITE" \
    -sql "
        SELECT
            $VERSION_ID AS version_id,
            'meri_' || etak_id AS authority_id,
            'meri_' || etak_id AS hydro_id,
            NULL AS geographical_name,
            muutmisaeg AS begin_lifespan_version,
            valjavote AS end_lifespan_version,
            NULL AS buffer,
            'Meri' AS local_type,
            NULL as source,
            ST_Simplify(ST_MakeValid($SOURCE_GEOMETRY_FIELD), 0) AS geometry
        FROM
            '$SOURCE_LAYER_NAME_MERI'
        WHERE
            $SOURCE_GEOMETRY_FIELD IS NOT NULL
    " \
    "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    $([ $SOURCE_PATH_MERI ] && echo "/vsizip//vsicurl/$SOURCE_PATH_MERI" || echo "/vsizip/$SOURCE_PATH_LOCAL_MERI")

echo "[*] Load 'seisuveekogu_a' objects from distant source..."
PGCLIENTENCODING=UTF8 ogr2ogr --debug ON \
    -f "PostgreSQL" \
    -nln "public.surface_water" \
    -append -update -progress \
    -gt 50000 \
    -dim 2 \
    -t_srs "EPSG:4258" \
    -dialect "SQLITE" \
    -sql "
        SELECT
            $VERSION_ID AS version_id,
            'seisuveekogu_a_' || etak_id AS authority_id,
            'seisuveekogu_a_' || etak_id AS hydro_id,
            nimetus AS geographical_name,
            muutmisaeg AS begin_lifespan_version,
            valjavote AS end_lifespan_version,
            NULL AS buffer,
            tyyp_t AS local_type,
            NULL as source,
            ST_Simplify(ST_MakeValid($SOURCE_GEOMETRY_FIELD), 0) AS geometry
        FROM
            '$SOURCE_LAYER_NAME_SEISUVEEKOGU_A'
        WHERE
            $SOURCE_GEOMETRY_FIELD IS NOT NULL
    " \
    "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    $([ $SOURCE_PATH_SEISUVEEKOGU_A ] && echo "/vsizip//vsicurl/$SOURCE_PATH_SEISUVEEKOGU_A" || echo "/vsizip/$SOURCE_PATH_LOCAL_SEISUVEEKOGU_A")

echo "[*] Load 'seisuveekogu_p' objects from distant source..."
PGCLIENTENCODING=UTF8 ogr2ogr --debug ON \
    -f "PostgreSQL" \
    -nln "public.surface_water" \
    -append -update -progress \
    -gt 50000 \
    -dim 2 \
    -t_srs "EPSG:4258" \
    -dialect "SQLITE" \
    -sql "
        SELECT
            $VERSION_ID AS version_id,
            'seisuveekogu_p_' || etak_id AS authority_id,
            'seisuveekogu_p_' || etak_id AS hydro_id,
            nimetus AS geographical_name,
            muutmisaeg AS begin_lifespan_version,
            valjavote AS end_lifespan_version,
            NULL AS buffer,
            tyyp_t AS local_type,
            NULL as source,
            ST_Simplify(ST_MakeValid($SOURCE_GEOMETRY_FIELD), 0) AS geometry
        FROM
            '$SOURCE_LAYER_NAME_SEISUVEEKOGU_P'
        WHERE
            $SOURCE_GEOMETRY_FIELD IS NOT NULL
    " \
    "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    $([ $SOURCE_PATH_SEISUVEEKOGU_P ] && echo "/vsizip//vsicurl/$SOURCE_PATH_SEISUVEEKOGU_P" || echo "/vsizip/$SOURCE_PATH_LOCAL_SEISUVEEKOGU_P")

echo "[*] Set version $VERSION_ID completion time..."
ogrinfo -q "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    -sql "
        UPDATE
            public.surface_water_version
        SET
            import_completed_at='$(date +"%Y-%m-%dT%H:%M:%S%z")',
            status='COMPLETED'
        WHERE
            id=$VERSION_ID
        "

end=$(date +%s)

echo "[!] Done in $((end-start)) seconds"