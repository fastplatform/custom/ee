# Region: ee/ESTONIA
# Object: protected_site
# Download URL: 
# S3 URL: https://oss.eu-west-0.prod-cloud-ocb.orange-business.com/fastplatform-tmp-public/ee/gis/protected_site/kkr_natura2000.zip/kkr_natura2000_loodusalad.shp
#
# This import script expects the following variables to be set:
# -------------------------------------------------------------
# VERSION_ID: ID of the version (in the target <object>_version table) that will be created
# POSTGRES_EXTERNAL_CONNECTION_STRING: connection string to the Postres external database
#                                      "PG:host='<HOST>' port='<PORT>' user='<USER>' password='<PASSWORD>' dbname='external'""
# SOURCE_PATH: URL to the zip file containing the shapefile(s)
# SOURCE_LAYER_NAME: layer name within the shapefile
# GEOMETRY_FIELD: geometry field name (usually "GEOMETRY" for shapefiles)

set -e

err_report() {
    echo "Failed on line $1"
}

trap 'err_report $LINENO' ERR

VERSION_ID="${VERSION_ID:-1000}"
POSTGRES_EXTERNAL_CONNECTION_STRING="${POSTGRES_EXTERNAL_CONNECTION_STRING:-"PG:host='localhost.fastplatform.eu' port='5438' user='fast' password='fast' dbname='external'"}"  # pragma: allowlist secret
SOURCE_PATH_LOCAL="$(pwd)/protected_site/kkr_natura2000.zip/kkr_natura2000_loodusalad.shp"
SOURCE_LAYER_NAME=${SOURCE_LAYER_NAME:-"kkr_natura2000_loodusalad"}
SOURCE_GEOMETRY_FIELD=${SOURCE_GEOMETRY_FIELD:-"GEOMETRY"}

start=$(date +%s)

echo "[*] Delete version $VERSION_ID if it already exists and (re)create it..."
ogrinfo -q "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    -sql "
        DELETE
            FROM public.protected_site_version
            WHERE id=$VERSION_ID;
        INSERT
            INTO public.protected_site_version(id, is_active, import_started_at)
            VALUES ($VERSION_ID, FALSE, '$(date +"%Y-%m-%dT%H:%M:%S%z")')
        "

echo "[*] Load objects from distant source..."
PGCLIENTENCODING=UTF8 ogr2ogr --debug ON \
    -f "PostgreSQL" \
    -nln "public.protected_site" \
    -append -update -progress \
    -gt 50000 \
    -dim 2 \
    -t_srs "EPSG:4258" \
    -dialect "SQLITE" \
    -sql "
        SELECT
            $VERSION_ID AS version_id,
            kr_kood AS authority_id,
            NULL AS legal_foundation_date,
            NULL AS legal_foundation_document,
            nimi AS site_name,
            NULL AS site_protection_classification,
            NULL AS source,
            ST_Simplify(ST_MakeValid($SOURCE_GEOMETRY_FIELD), 0) AS geometry
        FROM
            '$SOURCE_LAYER_NAME'
        WHERE
            $SOURCE_GEOMETRY_FIELD IS NOT NULL
    " \
    "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    $([ $SOURCE_PATH ] && echo "/vsizip//vsicurl/$SOURCE_PATH" || echo "/vsizip/$SOURCE_PATH_LOCAL")

echo "[*] Create designation types..."
ogrinfo -q "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
     -sql "
    INSERT INTO public.designation_type(percentage_under_designation, source, designation_id, designation_scheme_id) 
            VALUES (1, NULL, 'http://inspire.ec.europa.eu/codelist/Natura2000DesignationValue/siteOfCommunityImportance', 'http://inspire.ec.europa.eu/codelist/DesignationSchemeValue/natura2000'),
                   (1, NULL, 'http://inspire.ec.europa.eu/codelist/Natura2000DesignationValue/specialProtectionArea', 'http://inspire.ec.europa.eu/codelist/DesignationSchemeValue/natura2000'),
                   (1, NULL, 'http://inspire.ec.europa.eu/codelist/Natura2000DesignationValue/specialAreaOfConservation', 'http://inspire.ec.europa.eu/codelist/DesignationSchemeValue/natura2000')
            ON CONFLICT DO NOTHING;
    "

echo "[*] Assign SPA designation types..."
ogrinfo -q "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    -sql "
    INSERT INTO public.protected_site_site_designation(protectedsite_id, designationtype_id) 
            SELECT 
                protected_site.id AS protectedsite_id,
                (
                    SELECT
                        id
                    FROM
                        public.designation_type
                    WHERE
                        percentage_under_designation = 1
                        AND designation_id = 'http://inspire.ec.europa.eu/codelist/Natura2000DesignationValue/specialProtectionArea'
                        AND designation_scheme_id = 'http://inspire.ec.europa.eu/codelist/DesignationSchemeValue/natura2000'
                    LIMIT
                        1
                ) AS designationtype_id 
            FROM public.protected_site
            WHERE protected_site.version_id=$VERSION_ID;
    "

echo "[*] Set version $VERSION_ID completion time..."
ogrinfo -q "$POSTGRES_EXTERNAL_CONNECTION_STRING" \
    -sql "
        UPDATE
            public.protected_site_version
        SET
            import_completed_at='$(date +"%Y-%m-%dT%H:%M:%S%z")',
            status='COMPLETED'
        WHERE
            id=$VERSION_ID
        "

end=$(date +%s)

echo "[!] Done in $((end-start)) seconds"