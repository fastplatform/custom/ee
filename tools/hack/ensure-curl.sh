#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

check_curl() {

  if ! [ -x "$(command -v curl)" ]; then
    echo "curl not in path ! Please, install the curl binary"
    return 1
  fi
}

check_curl