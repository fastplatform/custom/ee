#!/bin/bash

git_rev=$(git rev-parse HEAD 2>/dev/null)
echo "BUILD_SCM_REVISION ${git_rev}"

git diff-index --quiet HEAD -- 2>/dev/null
if [[ $? == 0 ]];
then
    tree_status="Clean"
else
    tree_status="Modified"
fi
echo "BUILD_SCM_STATUS ${tree_status}"

GITSHA=$(git describe --always 2>/dev/null)

EE_CI_VERSION=${EE_CI_VERSION:=$(grep 'EE_CI_VERSION\s*=' VERSION | awk '{print $3}')}

if [[ -z "${VERSION}" ]]; then
  if [ "$(git describe --tags --exact-match ${git_rev} >/dev/null 2>&1; echo $?)" -eq 0 ]; then
    VERSION=$(git describe --tags)
  else
    VERSION="${EE_CI_VERSION}+${GITSHA}"
  fi
fi

echo "STABLE_EE_VERSION ${VERSION}"

EE_TAG=${VERSION/+/-}
if [ "$(git describe --tags --exact-match ${git_rev} >/dev/null 2>&1; echo $?)" -eq 0 ]; then
  EE_TAG=${EE_TAG}-release-${GITSHA}
  EE_TAG=${EE_TAG/+/-}
fi
echo "STABLE_EE_TAG ${EE_TAG}"
echo "STABLE_EE_TAG_PREFIXED_WITH_COLON :${EE_TAG}"

if [[ -z "${DOCKER_REGISTRY}" ]]; then
  DOCKER_REGISTRY="pwcfasteu.azurecr.io"
fi
if [[ -z "${DOCKER_IMAGE_PREFIX}" ]]; then
  DOCKER_IMAGE_PREFIX=fastplatform/custom/ee/
fi
echo "STABLE_DOCKER_REGISTRY ${DOCKER_REGISTRY}"
echo "STABLE_DOCKER_IMAGE_PREFIX ${DOCKER_IMAGE_PREFIX}"
