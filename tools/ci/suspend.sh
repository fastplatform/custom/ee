#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

source $(dirname $0)/../hack/ensure-kubectl.sh
source $(dirname $0)/common.sh


# Get the pgo-client pod
PGO_NAMESPACE=$(kubectl -n $NAMESPACE get ns $NAMESPACE -o=jsonpath='{.metadata.labels.pgo-installation-name}')
pgo_client_pod=$(kubectl -n $PGO_NAMESPACE get pod -l name=pgo-client -o NAME 2>/dev/null | sed 's,.*/,,g')
if [ -z "$pgo_client_pod" ]; then
    echo "No 'pgo-client' pod in PGO_NAMESPACE=${PGO_NAMESPACE}"
    return 1
fi
echo "[+] pgo-client found, $PGO_NAMESPACE/$pgo_client_pod"

# Delete all Postgres clusters
echo "[+] deleting Postgres clusters (keep data) ... "
pgclusters=$(kubectl -n $NAMESPACE get pgcluster -o NAME 2>/dev/null | sed 's,.*/,,g')
for pgcluster in $pgclusters; do 
  kubectl -n $PGO_NAMESPACE exec -t $pgo_client_pod -- pgo delete cluster $pgcluster -n $NAMESPACE --keep-data --keep-backups --no-prompt
done
echo OK

# Delete all workloads
echo "[+] deleting all remaining workloads ... "
workloads="ksvc statefulset deploy"
for w in  $workloads; do
  kubectl -n $NAMESPACE get $w -l "!serving.knative.dev/service,!pg-cluster" -o NAME | xargs -n 1 kubectl -n $NAMESPACE delete 2>/dev/null || true # ignore if empty
done
echo OK

echo -n "[+] waiting for all pods to stop  ... "
timeout=600
while [ ! -z "$(kubectl -n $NAMESPACE get pods -o NAME)" ]; do
  sleep 1;
  timeout=$((timeout-1))
  if [[ $timeout -le 0 ]]; then
    echo "pods still remaining, timeout !"
    return 1
  fi
done
echo OK

# Delete all workloads
echo "[+] deleting all jobs ... "
workloads="job"
for w in  $workloads; do
  kubectl -n $NAMESPACE get $w -o NAME | xargs -n 1 kubectl -n $NAMESPACE delete 2>/dev/null || true # ignore if empty
done
echo OK

# Delete all unnecessary volumes
echo "[+] deleting all unnecessary volumes ... "
kubectl -n $NAMESPACE get pvc -l "!pg-cluster,redis-cluster!=mobile-push-notification" -o NAME | xargs -n 1 kubectl -n $NAMESPACE delete 2>/dev/null || true # ignore if empty
echo OK
