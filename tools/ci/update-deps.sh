#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

source $(dirname $0)/../hack/ensure-curl.sh
source $(dirname $0)/common.sh

if [ ! -z "${GITLAB_READ_ACCESS_TOKEN}" ]; then
  params="private_token=${GITLAB_READ_ACCESS_TOKEN}"
  
  deps_step_1_path=$(dirname $0)/../repositories/deps-step-1.bzl

  # core
  core_project_id="20397766"
  current_sha256=$(bazel query //external:fastplatform_core_artifacts --output build 2>/dev/null | \
    grep sha256 | \
    tr -d '[[:space:]],' | \
    sed -e 's/sha256="\(.*\)"/\1/g')
  sed -i.bak \
    "s,${core_project_id}/jobs/[0-9][0-9]*/.*[^\"],${core_project_id}/jobs/artifacts/master/download?job=publish%3Aall,g" \
    $deps_step_1_path
  new_sha256=$(curl -L --silent \
    "$(cat $deps_step_1_path | grep -Eo https://.*/${core_project_id}/[^\"]*)&${params}" | \
    shasum -a 256 -b | cut -d' ' -f 1)
  sed -i.bak "s,${current_sha256},${new_sha256},g" $deps_step_1_path

  # addons/arc
  arc_project_id="20197857"
  current_sha256=$(bazel query //external:fastplatform_addons_arc_artifacts --output build 2>/dev/null | \
    grep sha256 | \
    tr -d '[[:space:]],' | \
    sed -e 's/sha256="\(.*\)"/\1/g')
  sed -i.bak \
    "s,${arc_project_id}/jobs/[0-9][0-9]*/.*[^\"],${arc_project_id}/jobs/artifacts/master/download?job=publish%3Aall,g" \
    $deps_step_1_path
  new_sha256=$(curl -L --silent \
    "$(cat $deps_step_1_path | grep -Eo https://.*/${arc_project_id}/[^\"]*)&${params}" | \
    shasum -a 256 -b | cut -d' ' -f 1)
  sed -i.bak "s,${current_sha256},${new_sha256},g" $deps_step_1_path

  # addons/ilmateenistus
  ilmateenistus_project_id="20376285"
  current_sha256=$(bazel query //external:fastplatform_addons_ilmateenistus_artifacts --output build 2>/dev/null | \
    grep sha256 | \
    tr -d '[[:space:]],' | \
    sed -e 's/sha256="\(.*\)"/\1/g')
  sed -i.bak \
    "s,${ilmateenistus_project_id}/jobs/[0-9][0-9]*/.*[^\"],${ilmateenistus_project_id}/jobs/artifacts/master/download?job=publish%3Aall,g" \
    $deps_step_1_path
  new_sha256=$(curl -L --silent \
    "$(cat $deps_step_1_path | grep -Eo https://.*/${ilmateenistus_project_id}/[^\"]*)&${params}" | \
    shasum -a 256 -b | cut -d' ' -f 1)
  sed -i.bak "s,${current_sha256},${new_sha256},g" $deps_step_1_path

  # mobile/farmer-mobile-app
  farmer_mobile_app_project_id="16049031"
  current_sha256=$(bazel query //external:fastplatform_farmer_mobile_app_artifacts --output build 2>/dev/null | \
    grep sha256 | \
    tr -d '[[:space:]],' | \
    sed -e 's/sha256="\(.*\)"/\1/g')
  sed -i.bak \
    "s,${farmer_mobile_app_project_id}/jobs/[0-9][0-9]*/.*[^\"],${farmer_mobile_app_project_id}/jobs/artifacts/master/download?job=publish%3Aall,g" \
    $deps_step_1_path
  new_sha256=$(curl -L --silent \
    "$(cat $deps_step_1_path | grep -Eo https://.*/${farmer_mobile_app_project_id}/[^\"]*)&${params}" | \
    shasum -a 256 -b | cut -d' ' -f 1)
  sed -i.bak "s,${current_sha256},${new_sha256},g" $deps_step_1_path
fi