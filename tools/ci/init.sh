#!/bin/bash

source $(dirname $0)/common.sh


function init_services() {
    namespace=$1
    module=$2

    case $module in
        core)
            knative_service_exec $namespace "web-backend" python ../../*.binary init /init-data/external --db-alias external --clear 
            knative_service_exec $namespace "web-backend" python ../../*.binary init /init-data/fastplatform --clear
            ;;

        *)
            return
            ;;
    esac
}
