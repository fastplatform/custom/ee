load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@io_bazel_rules_docker//container:container.bzl", "container_pull")
load("@io_bazel_rules_docker//python3:image.bzl", _py_image_repos = "repositories")
load("@io_bazel_rules_docker//repositories:deps.bzl", container_deps = "deps")
load("@rules_python_external//:defs.bzl", "pip_install")

def transitive_deps():
    _py_image_repos()

def docker_deps():
    container_deps()
    maybe(
        container_pull,
        name = "py3.7_image_base",
        registry = "gcr.io",
        repository = "distroless/python3-debian10",
        digest = "sha256:025b77e95e701917434c478e7fd267f3d894db5ca74e5b2362fe37ebe63bbeb0",
    )
    maybe(
        container_pull,
        name = "py3.7_debug_image_base",
        registry = "gcr.io",
        repository = "distroless/python3-debian10",
        digest = "sha256:ebd73d8f4da293c9826e8646137a05260ecd1b7ee103cb1f62ebf010fda7c7f9",
    )

def fastplatform_deps():
    maybe(
        http_archive,
        name = "fastplatform_core_artifacts",
        urls = [
            "https://gitlab.com/api/v4/projects/20397766/jobs/1476377880/artifacts.zip"
        ],
        type = "zip",
        auth_patterns = {
            "gitlab.com": "Bearer <password>",
        },
        build_file_content = "exports_files([\"release_kustomize_manifest.tar.gz\",\"query_collections.yaml\"])",
        sha256 = "8f2da290225042767756ed379de30659edcf75d8255809a7cd41e74cc29945ec",
    )
    maybe(
        http_archive,
        name = "fastplatform_addons_arc_artifacts",
        urls = [
            "https://gitlab.com/api/v4/projects/20197857/jobs/1102248853/artifacts.zip"
        ],
        type = "zip",
        auth_patterns = {
            "gitlab.com": "Bearer <password>",
        },
        build_file_content = "exports_files([\"release_kustomize_manifest.tar.gz\",\"query_collections.yaml\"])",
        sha256 = "b20b87173d7713c85d3a31e79690a6751fb44a1a6efd4b1475388ab1f5d44b18",
    )
    maybe(
        http_archive,
        name = "fastplatform_addons_ilmateenistus_artifacts",
        urls = [
            "https://gitlab.com/api/v4/projects/20376285/jobs/1102289112/artifacts.zip"
        ],
        type = "zip",
        auth_patterns = {
            "gitlab.com": "Bearer <password>",
        },
        build_file_content = "exports_files([\"release_kustomize_manifest.tar.gz\",\"query_collections.yaml\"])",
        sha256 = "9cb84c739ae5236b3c5eda94438cc7832fcefb577d34f5c0f5c93f6784eaa356",
    )    
    maybe(
        http_archive,
        name = "fastplatform_farmer_mobile_app_artifacts",
        urls = [
            "https://gitlab.com/api/v4/projects/16049031/jobs/1116964986/artifacts.zip"
        ],
        type = "zip",
        auth_patterns = {
            "gitlab.com": "Bearer <password>",
        },
        build_file_content = "exports_files([\"query_collections.yaml\"])",
        sha256 = "414b4b0299229478b700daa6234a478aa2f220e083c1ee536da4330ce78a4336",
    )

def pip_deps():
    maybe(
        pip_install,
        name = "iacs_pria_pip",
        requirements = "//services/iacs/pria:requirements.txt",
    )
    maybe(
        pip_install,
        name = "idp_authentication_pip",
        requirements = "//services/idp/authentication:requirements.txt",
    )
    maybe(
        pip_install,
        name = "idp_authentication_pip_dev",
        requirements = "//services/idp/authentication:requirements-dev.txt",
    )

def deps_step_1():
    transitive_deps()
    docker_deps()
    fastplatform_deps()
    pip_deps()

