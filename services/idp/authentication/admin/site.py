from django.contrib import admin


class FaSTAdminSite(admin.AdminSite):
    name = 'admin_site'
    site_header = 'FaST'
    site_title = "FaST Administration"
    site_url = None
    index_title = 'Management console'

    index_template = 'admin/index.html'
    app_index_template = 'admin/app_index.html'


admin_site = FaSTAdminSite()
