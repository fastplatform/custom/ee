Django==3.0.2
django-admin-interface==0.12.2
django-user-sessions==1.7.1
mozilla-django-oidc==1.2.4
opentelemetry-exporter-jaeger==0.13b0
opentelemetry-instrumentation-django==0.13b0
opentelemetry-instrumentation-psycopg2==0.13b0
# TODO: use pypi package when MR https://github.com/open-telemetry/opentelemetry-python/pull/1148 will be released
-e "git+https://gitlab.com/fastplatform/pypi/opentelemetry-python/#egg=opentelemetry-sdk&subdirectory=opentelemetry-sdk"
psycopg2-binary==2.8.5
Pillow==7.1.2
PyJWT==1.7.1
requests==2.23.0
setuptools==50.3.0 # get pkg_resources package for pydantic