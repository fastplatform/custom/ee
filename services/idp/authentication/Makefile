# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

include service.env
export $(shell sed 's/=.*//' service.env)
include secrets.env
export $(shell sed 's/=.*//' secrets.env)

# START
.PHONY: start
start: ## start
	python manage.py runserver 0.0.0.0:8001

# SSL START
starts: ## start with ssl
	python3 manage.py runsslserver 0.0.0.0:48001 --certificate $(shell pwd)/certificates/localhost.fastplatform.eu/fullchain1.pem --key $(shell pwd)/certificates/localhost.fastplatform.eu/privkey1.pem

create_superuser:
	echo "from authentication.models import User; User.objects.create_superuser('admin', 'tech@fastplatform.eu', 'azerty')" | python3 manage.py shell

# START OP_CYL REDIRECT PROXY
start_op_cyl_redirect_proxy: ## start op-cyl redirect proxy
	@docker run --name op_cyl_auth_redirect_proxy\
		-v $(shell pwd)/extras/op_cyl_redirect.conf:/etc/nginx/nginx.conf:ro \
		-v $(shell pwd)/extras/certificates/localhost.fastplatform.eu/fullchain1.pem:/etc/nginx/publickey.pem:ro \
		-v $(shell pwd)/extras/certificates/localhost.fastplatform.eu/privkey1.pem:/etc/nginx/privatekey.pem:ro \
		-p 48001:48001 \
		-d nginx

stop_op_cyl_redirect_proxy: ## start op-cyl redirect proxy
	-docker stop op_cyl_auth_redirect_proxy
	-docker rm op_cyl_auth_redirect_proxy

bazel-run: ## 
	bazel run :authentication-run
