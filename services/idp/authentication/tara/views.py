from authentication.utils import import_from_settings
from django.conf import settings
from django.contrib import messages, auth
from django.contrib.auth import get_user_model, login
from django.core.exceptions import SuspiciousOperation
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import View
from mozilla_django_oidc.auth import OIDCAuthenticationBackend
from mozilla_django_oidc.views import OIDCAuthenticationCallbackView

import jwt
import random
import requests
import string
import logging

LOGGER = logging.getLogger(__name__)

class TaraOIDCAB(OIDCAuthenticationBackend):
    def filter_users_by_claims(self, claims):
        """Return all users matching the specified username."""
        username = claims.get('sub')
        if not username:
            return self.UserModel.objects.none()
        return self.UserModel.objects.filter(username__iexact=username)

    def create_user(self, claims):
        username = claims.get('sub')
        first_name = claims.get('given_name')
        last_name = claims.get('family_name')
        name = first_name + ' ' + last_name
        user = self.UserModel.objects.create_user(username=username, first_name=first_name, last_name=last_name, name=name, is_staff=False, is_superuser=False)
        user.save()

        return user


    def update_user(self, user, claims):
        user.username = claims.get('sub')
        user.first_name = claims.get('given_name')
        user.last_name = claims.get('family_name')
        user.save()

        return user


class TaraOIDCACV(OIDCAuthenticationCallbackView):

    def get(self, request):
        """Callback handler for OIDC authorization code flow"""

        if request.GET.get('error'):
            # Ouch! Something important failed.
            # Make sure the user doesn't get to continue to be logged in
            # otherwise the refresh middleware will force the user to
            # redirect to authorize again if the session refresh has
            # expired.
            if request.user.is_authenticated:
                auth.logout(request)
            assert not request.user.is_authenticated
        elif 'code' in request.GET and 'state' in request.GET:

            # Check instead of "oidc_state" check if the "oidc_states" session key exists!
            if 'oidc_states' not in request.session:
                return self.login_failure()

            # State and Nonce are stored in the session "oidc_states" dictionary.
            # State is the key, the value is a dictionary with the Nonce in the "nonce" field.
            state = request.GET.get('state')
            if state not in request.session['oidc_states']:
                msg = 'OIDC callback state not found in session `oidc_states`!'
                raise SuspiciousOperation(msg)

            # Get the nonce from the dictionary for further processing and delete the entry to
            # prevent replay attacks.
            nonce = request.session['oidc_states'][state]['nonce']
            del request.session['oidc_states'][state]

            # Authenticating is slow, so save the updated oidc_states.
            request.session.save()
            # Reset the session. This forces the session to get reloaded from the database after
            # fetching the token from the OpenID connect provider.
            # Without this step we would overwrite items that are being added/removed from the
            # session in parallel browser tabs.
            request.session = request.session.__class__(request.session.session_key)

            kwargs = {
                'request': request,
                'nonce': nonce,
            }

            self.user = auth.authenticate(**kwargs)

            if self.user and self.user.is_active:
                from django.contrib.auth import login
                login(request, self.user)
                return redirect(settings.AUTHENTICATION_REDIRECT_URL)

        return self.login_failure()

class TaraAuthenticationView(View):
    """Init the Op Cyl authentication flow to get a JWT token"""

    http_method_names = ['get']

    def get(self, request):
        return redirect(settings.LOGIN_URL)


class TaraAuthenticationCallbackView(View):
    """ Once the token is send back,
     - check it through the op cyl api
     - if correct, create the user if needed and log in
     - redirect to the authentication_callback view 
    """

    http_method_names = ['get']

    def __init__(self, *args, **kwargs):
        self.AUTHENTICATION_REDIRECT_URL = import_from_settings('AUTHENTICATION_REDIRECT_URL')

    def get(self, request):
        # error_message = "An error occurred while running authentication process. Please try to login again."
        error_message = "Autentimisprotsessi käivitamisel ilmnes viga. Proovige uuesti sisse logida."
        if request.user.is_active:
            login(request, request.user, 'django.contrib.auth.backends.ModelBackend')
            return redirect(self.AUTHENTICATION_REDIRECT_URL)
        else:
            messages.error(request, error_message)
            return redirect(self.AUTHENTICATION_REDIRECT_URL + '?federated_provider=tara')