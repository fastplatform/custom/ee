from django.contrib.auth import views as auth_views
from django.urls import include, path
from . import views
from os import environ

urlpatterns = [
    path('', views.TaraAuthenticationView.as_view(), name='index'),
    path('callback', views.TaraAuthenticationCallbackView.as_view(), name='callback')
]