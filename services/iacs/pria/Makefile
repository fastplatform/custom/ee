# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

include service.env
export $(shell sed 's/=.*//' service.env)
include secrets.env
export $(shell sed 's/=.*//' secrets.env)

# Start ilmateenistus locally
.PHONY: start
start: ## Start the IACS/ARPEA service locally on port 7777
	uvicorn \
	app.main:app \
	--host=0.0.0.0 \
	--port=7777 \
	--reload \
	--log-config logging-dev.json

USER=admin
test-insert-demo-holding:  # Insert the demo holding for user USER
	curl -X POST \
		-d '$(shell cat app/tests/insert_demo_holding.json)' \
		-H "Authorization: Service $(API_GATEWAY_SERVICE_KEY)" \
		-H "X-Hasura-User-Id: $(USER)" \
		-H "Content-Type: application/json" \
		http://localhost:7777/graphql

test-sync-holdings:  # Sync holdings from the ARPEA IACS database for user USER
	curl -X POST \
		-d '$(shell cat app/tests/sync_holdings.json)' \
		-H "Authorization: Service $(API_GATEWAY_SERVICE_KEY)" \
		-H "X-Hasura-User-Id: $(USER)" \
		-H "Content-Type: application/json" \
		http://localhost:7777/graphql

HOLDING_ID=""
test-sync-holding:  # Sync the plots of holding HOLDING_ID and user USER from the ARPEA database
	curl -X POST \
		-d '$(shell jq '.variables.holding_id = "$(HOLDING_ID)"' << cat app/tests/sync_holding.json)' \
		-H "Authorization: Service $(API_GATEWAY_SERVICE_KEY)" \
		-H "X-Hasura-User-Id: $(USER)" \
		-H "Content-Type: application/json" \
		http://localhost:7777/graphql

start-tracing: ## Start the Jaeger tracing service
	docker run --rm --name tracing-fastplatform \
		-p 5775:5775/udp \
		-p 16686:16686 \
		-d docker.io/jaegertracing/all-in-one 

stop-tracing: ## Stop the Jaeger tracing service
	-docker stop tracing-fastplatform

bazel-run: ## 
	bazel run .
