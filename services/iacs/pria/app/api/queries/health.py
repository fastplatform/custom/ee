import graphene


class HealthQuery(graphene.ObjectType):

    healthz = graphene.String(default_value='ok')
