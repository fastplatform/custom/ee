import graphene

from app.api.queries.health import HealthQuery
from app.api.queries.name import NameQuery


class Query(HealthQuery, NameQuery, graphene.ObjectType):
    pass
