import graphene

from app.api.mutations.sync_holdings import SyncHoldings
from app.api.mutations.sync_holding import SyncHolding
from app.api.mutations.insert_demo_holding import InsertDemoHolding
from app.api.mutations.duplicate_holding_campaign import DuplicateHoldingCampaign


class Mutation(graphene.ObjectType):
    sync_holdings = SyncHoldings.Field()
    sync_holding = SyncHolding.Field()
    insert_demo_holding = InsertDemoHolding.Field()
    duplicate_holding_campaign = DuplicateHoldingCampaign.Field()
