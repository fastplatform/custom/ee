import logging

import graphene
from gql import gql
from graphql import GraphQLError

from opentelemetry import trace

from shapely import wkt, ops
import pyproj
from shapely.geometry.multipolygon import MultiPolygon

from app.settings import config
from app.api.lib.xroad_pria.xroad_pria import xroad_pria_client
from app.db.graphql_clients import fastplatform

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


XMLTODICT_NAMESPACES = {
    "http://schemas.xmlsoap.org/soap/envelope/": "soapenv",
    "http://x-road.eu/xsd/identifiers": "id",
    "http://x-road.eu/xsd/xroad.xsd": "xrd",
    "http://toetusteregister.x-road.eu": "tns",
}


class SyncHolding(graphene.Mutation):
    """Mutation to pull the holding's latest data from the IACS system and
    upsert this data into a HoldingCampaign.

    The mutation expects the X-Hasura-User-Id header to be set to the ID of the
    requesting user

    Args:
        holding_id (str): Holding
        campaign_id (int): Target campaign
        iacs_year (int): Source IACS year (if supported by the IACS APIs)

    Raises:
        GraphQLError("HOLDING_DOES_NOT_EXIST):
            The requested holding does not exist or the user is not a related
            party to this holding and therefore cannot edit it
        GraphQLError("IACS_REMOTE_ERROR):
            There was an error while retrieving data from the distant IACS API
        GraphQLError("IACS_DATA_ERROR):
            There was an error while parsing the data received from the distant
            IACS API

    Returns:
        A sync_holding GraphQL node containing:
        - plots_ids: Authority IDs of the plots that have been upserted
    """

    class Arguments:
        holding_id = graphene.String(required=True)
        campaign_id = graphene.Int(required=True)
        iacs_year = graphene.Int(required=False)

    plot_ids = graphene.List(graphene.String)

    async def mutate(self, info, holding_id, campaign_id, iacs_year=None):

        request = info.context["request"]

        # This will fail if Hasura has not injected the User-Id header
        user_id = request.headers["X-Hasura-User-Id"]

        # Retrieve info about the holding we are supposed to gather
        # plots for
        with tracer().start_as_current_span("query_holding_info"):
            logger.debug("query_holding_info")

            query = (config.API_DIR / "graphql/query_holding_by_id.graphql").read_text()
            variables = {"holding_id": holding_id}

            # Impersonate the user
            extra_args = {
                "headers": {
                    "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
                    "X-Hasura-User-Id": request.headers["X-Hasura-User-Id"],
                    "X-Hasura-Role": request.headers["X-Hasura-Role"],
                }
            }

            response = await fastplatform.execute(
                gql(query), variables, extra_args=extra_args
            )

            # Holding does not exist or the requesting user is not a
            # related party to the holding (in both cases: empty response)
            if not response["holding"]:
                raise GraphQLError("HOLDING_DOES_NOT_EXIST")

            holding_name = response["holding"][0]["name"]

        # Send request to PRIA endpoint
        with tracer().start_as_current_span("query_pria_api"):
            logger.debug("query_pria_api")
            fields = await xroad_pria_client.get_application_fields(
                user_id=user_id,
                holding_id=holding_id,
                # TODO set iacs_year=iacs_year when the parameter is not sent anymore
                # by the app
                iacs_year=None,
            )

            if not len(fields):
                raise GraphQLError("IACS_REMOTE_RESPONSE_EMPTY")

        # Map the plots the FaST data model
        with tracer().start_as_current_span("map_and_reproject_plots"):
            logger.debug("map_and_reproject_plots")

            plots = []
            projections = {}
            for field in fields:
                iacs_land_cover_id = field.kultuuriKood.text

                # If there is a county (maakond) for this plot, then
                # convert this county_id into the FaST administrative levels
                # structure (see here: https://gitlab.com/fastplatform/custom/ee/-/blob/master/init/fastplatform/200.administrative_unit.csv)
                if hasattr(field, "maakond"):
                    county_id = f"EE-{int(field.maakond.text):0>4d}"
                else:
                    county_id = None

                if hasattr(field, "polluId"):
                    authority_id = f"{config.AUTHORITY_ID_PREFIX}:{field.polluId.text}"
                else:
                    authority_id = None

                # The user identifies plot from this data: MASSIIVINR + POLLUNR + MAAALANR
                massiivi_nr = getattr(field, "massiiviNr", "-")
                pollu_nr = getattr(field, "polluNr", "-")
                plot_name = f"{config.AUTHORITY_ID_PREFIX}/{massiivi_nr}/{pollu_nr}"
                if hasattr(field, "maaAlaNr"):
                    plot_name + f"/{field.maaAlaNr}"

                plot = {
                    "authority_id": authority_id,
                    "name": plot_name,
                    "plant_variety_ids": [iacs_land_cover_id],
                    "address": {
                        "administrative_unit_level_1_id": "EE",  # Estonia
                        "administrative_unit_level_2_id": county_id,  # County
                    },
                }

                # Parse the geometry and reproject to ETRS89 if needed
                try:
                    geometry_wkt = field.geomeetria.text
                    srid = field.geomeetria.attrib["srsName"]
                except:
                    logger.exception(
                        f"Missing geometry or SRID for field {authority_id}, this plot will be ignored"
                    )
                    continue

                if srid not in projections:
                    proj_from = pyproj.CRS(
                        srid
                    )  # We expect to receive the SRID as 'EPSG:XXXX'
                    proj_to = pyproj.CRS("EPSG:4258")
                    projections[srid] = pyproj.Transformer.from_crs(
                        proj_from, proj_to, always_xy=True
                    ).transform

                geometry = wkt.loads(geometry_wkt)
                geometry = ops.transform(projections[srid], geometry)

                # Convert Polygon types to MultiPolygon (geometry type for Plot in the database)
                if geometry.geom_type == "Polygon":
                    geometry = MultiPolygon([geometry])

                # Convert to a __geo_interface__ dict and assign CRS
                geometry = geometry.__geo_interface__
                geometry["crs"] = {"type": "name", "properties": {"name": "EPSG:4258"}}

                plot["geometry"] = geometry

                plots += [plot]

            if not plots:
                raise GraphQLError("IACS_REMOTE_RESPONSE_EMPTY")

        with tracer().start_as_current_span("insert_holding_campaign"):
            logger.debug("insert_holding_campaign")

            holding_campaigns = [
                {
                    "campaign_id": campaign_id,
                    "holding_id": holding_id,
                    "sites": {
                        "data": [
                            {
                                "authority_id": holding_id,  # Single site
                                "name": holding_name,  # Single site
                                "plots": {
                                    "data": [
                                        {
                                            "authority_id": plot["authority_id"],
                                            "name": plot["name"],
                                            "geometry": plot["geometry"],
                                            "plot_plant_varieties": {
                                                "data": [
                                                    {"plant_variety_id": pvi}
                                                    for pvi in plot["plant_variety_ids"]
                                                ],
                                                "on_conflict": {
                                                    "constraint": "plot_plant_variety_plot_id_plant_variety_id_unique",
                                                    "update_columns": [
                                                        "plot_id",
                                                        "plant_variety_id",
                                                    ],
                                                },
                                            },
                                            "address": {
                                                "data": plot["address"],
                                                "on_conflict": {
                                                    "constraint": "address_pkey",
                                                    "update_columns": [
                                                        "administrative_unit_level_1_id",
                                                        "administrative_unit_level_2_id",
                                                        "administrative_unit_level_3_id",
                                                        "administrative_unit_level_4_id",
                                                        "administrative_unit_level_5_id",
                                                        "administrative_unit_level_6_id",
                                                    ],
                                                },
                                            },
                                        }
                                        for plot in plots
                                    ],
                                    "on_conflict": {
                                        "constraint": "plot_authority_id_site_id_unique",
                                        "update_columns": [
                                            "authority_id",
                                            "site_id",
                                            "name",
                                            "geometry",
                                        ],
                                    },
                                },
                            }
                        ],
                        "on_conflict": {
                            "constraint": "site_authority_id_holding_campaign_id_unique",
                            "update_columns": [
                                "authority_id",
                                "holding_campaign_id",
                                "name",
                            ],
                        },
                    },
                }
            ]

            # (config.API_DIR / "holding_campaigns.json").write_text(
            #     json.dumps(holding_campaigns, indent=4)
            # )

            mutation = (
                config.API_DIR / "graphql/mutation_insert_holding_campaigns.graphql"
            ).read_text()
            response = await fastplatform.execute(
                gql(mutation),
                {
                    "holding_campaigns": holding_campaigns,
                },
            )

        return SyncHolding(plot_ids=[plot["authority_id"] for plot in plots])
