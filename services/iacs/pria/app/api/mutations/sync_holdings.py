import logging
import json

import graphene
from gql import gql
from opentelemetry import trace
from graphql import GraphQLError
from stdnum.exceptions import ValidationError

from app.settings import config
from app.api.lib.xroad_pria.xroad_pria import xroad_pria_client

from app.db.graphql_clients import fastplatform

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class SyncHoldings(graphene.Mutation):
    """Mutation to pull all the holding IDs that the user is allowed to see
    in the IACS system

    The mutation expects the X-Hasura-User-Id header to be set to the ID of the
    requesting user.

    The mutation will upsert the corresponding Holding objects in the database,
    as well as upsert the necessary UserRelatedParty objects to grant the user
    access to these holdings.

    Because of the way the data is returned by the PRIA API, FaST cannot know
    the role of the user in each of the holdings (farmer or advisor or public
    institution), hence for Estonia, all UserRelatedParty objects created by
    this service bear the role "farmer".

    Args:
        (none, as the user_id is supposed to be in the headers)

    Raises:
        GraphQLError("IACS_REMOTE_ERROR):
            There was an error while retrieving data from the distant IACS API
        GraphQLError("IACS_DATA_ERROR):
            There was an error while parsing the data received from the distant
            IACS API

    Returns:
        A sync_holdings GraphQL node containing:
        - holding_ids: Authority IDs of the plots that have been upserted
    """

    class Arguments:
        pass

    holding_ids = graphene.List(graphene.String)

    async def mutate(self, info):

        request = info.context["request"]

        # This will fail if Hasura has not injected the User-Id header
        user_id = request.headers["X-Hasura-User-Id"]

        with tracer().start_as_current_span("query_pria_api"):
            logger.debug("query_pria_api")

            # Request the access grants for the user
            try:
                grants = await xroad_pria_client.get_access_grants(user_id)
            except ValidationError:
                # If the user_id is wrong (ie not a real Estonian isikukood)
                # then we just log an error and return nothing
                # This is to be able to deal with manual accounts like john.doe
                # without throwing ununderstable errors in the mobile app
                logger.exception(f"Invalid input for username {user_id}")
                return SyncHoldings(holding_ids=[])

        with tracer().start_as_current_span("convert_access_grants_to_holdings"):
            logger.debug("convert_access_grants_to_holdings")

            # Convert the access grants list to the FaST holding semantics
            holdings = []
            for grant in grants:

                registry_code = (
                    grant.registrikood.text if hasattr(grant, "registrikood") else None
                )
                personal_code = (
                    grant.isikukood.text if hasattr(grant, "isikukood") else None
                )

                if registry_code is None and personal_code is None:
                    logging.error(
                        f"Invalid grant, no holding_id could be derived from {json.dumps(grant)}"
                    )
                    raise GraphQLError("IACS_DATA_ERROR")

                elif registry_code is not None:
                    # In case the holding is a company, we use the company registry code as the holding_id
                    # and the company name as the holding_name
                    holding_id = registry_code
                    holding_name = grant.juriidilineNimi.text

                else:
                    # If the registry code is absent, then the holding is attached to a natural person
                    # and we use the personal ID of this person as the holding_id
                    holding_id = personal_code
                    first_name = grant.eesnimi.text
                    last_name = grant.perekonnanimi.text

                    if first_name or last_name:
                        holding_name = " ".join([first_name, last_name]).strip()
                    else:
                        holding_name = f"{config.AUTHORITY_ID_PREFIX}:{holding_id}"

                # Default role for the user in this farm is to be 'farmer' as the PRIA access grants
                # API does not return any indication of the role of each user within the farm
                user_role = "farmer"

                holdings.append(
                    {
                        "id": holding_id,
                        "name": holding_name,
                        "user_related_parties": [
                            {"user_id": user_id, "role": user_role}
                        ],
                    }
                )

        # Insert the holdings and set this user as a related party
        # Note that this will fail (due to foreign key constraint violation)
        # if the user does not exist
        with tracer().start_as_current_span("insert_holdings"):
            logger.debug("insert_holdings")

            # Convert holdings to upsert statements
            objects = [
                {
                    "id": holding["id"],
                    "name": holding["name"],
                    "user_related_parties": {
                        "data": [
                            {"role": user["role"], "user_id": user["user_id"]}
                            for user in holding["user_related_parties"]
                        ],
                        "on_conflict": {
                            "constraint": "user_related_party_holding_id_user_id_unique",
                            "update_columns": ["role"],
                        },
                    },
                }
                for holding in holdings
            ]

            mutation = (
                config.API_DIR / "graphql/mutation_insert_holdings.graphql"
            ).read_text()
            response = await fastplatform.execute(gql(mutation), {"objects": objects})

            return SyncHoldings(holding_ids=[o["id"] for o in objects])
