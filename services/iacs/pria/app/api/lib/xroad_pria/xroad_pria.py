import uuid
import logging

import httpx
from graphql import GraphQLError
from lxml import objectify, etree
from stdnum.ee import ik
from stdnum.exceptions import ValidationError

from app.settings import config

logger = logging.getLogger(__name__)


class XRoadPRIAClient:
    def __init__(self):

        self.http_client = None

        self.access_grants_request_template = (
            config.API_DIR
            / "lib/xroad_pria/xroad_pria_access_grants_request_template.xml"
        ).read_text()

        self.application_fields_request_template = (
            config.API_DIR
            / "lib/xroad_pria/xroad_pria_application_fields_request_template.xml"
        ).read_text()

        self.XMLTODICT_NAMESPACES = {
            "http://schemas.xmlsoap.org/soap/envelope/": "soapenv",
            "http://x-road.eu/xsd/identifiers": "id",
            "http://x-road.eu/xsd/xroad.xsd": "xrd",
            "http://toetusteregister.x-road.eu": "tns",
        }

        self.COUNTRY_ISO_2_TO_3 = {
            "AT": "AUT",
            "BE": "BEL",
            "BG": "BGR",
            "HR": "HRV",
            "CY": "CYP",
            "CZ": "CZE",
            "DK": "DNK",
            "EE": "EST",
            "FI": "FIN",
            "FR": "FRA",
            "DE": "DEU",
            "GR": "GRC",
            "HU": "HUN",
            "IE": "IRL",
            "IT": "ITA",
            "LV": "LVA",
            "LT": "LTU",
            "LU": "LUX",
            "MT": "MLT",
            "NL": "NLD",
            "PO": "POL",
            "PT": "PRT",
            "RO": "ROU",
            "SK": "SVK",
            "SI": "SVN",
            "ES": "ESP",
            "SE": "SWE",
        }

    def create_http_client(self):
        self.http_client = httpx.AsyncClient(
            verify=config.XROAD_SECURITY_SERVER_PUBLIC_KEY_PATH
        )

    async def close_http_client(self):
        await self.http_client.aclose()

    def make_access_grants_request_body(self, user_id):
        logger.debug("make_access_grants_request_body")

        # Split user_id to number and ISO3 country code
        # "EE123456789" --> ("EST", "123456789")
        try:
            country_iso_2 = user_id[:2]
            user_id_no_country = user_id[2:]
            country_iso_3 = self.COUNTRY_ISO_2_TO_3[country_iso_2]
        except KeyError:
            user_id_no_country = user_id
            country_iso_3 = 'EST'

        if country_iso_3 == 'EST':
            ik.validate(user_id_no_country)

        return self.access_grants_request_template.format(
            xroad_user_id=user_id,
            xroad_id=uuid.uuid4(),
            # Client header
            client_xroad_instance=config.XROAD_FAST_CLIENT_XROAD_INSTANCE,
            client_member_class=config.XROAD_FAST_CLIENT_MEMBER_CLASS,
            client_member_code=config.XROAD_FAST_CLIENT_MEMBER_CODE,
            client_subsystem_code=config.XROAD_FAST_CLIENT_SUBSYSTEM_CODE,
            # Service header
            service_xroad_instance=config.XROAD_ACCESS_GRANTS_SERVICE_X_ROAD_INSTANCE,
            service_member_class=config.XROAD_ACCESS_GRANTS_SERVICE_MEMBER_CLASS,
            service_member_code=config.XROAD_ACCESS_GRANTS_SERVICE_MEMBER_CODE,
            service_subsystem_code=config.XROAD_ACCESS_GRANTS_SERVICE_SUBSYSTEM_CODE,
            service_service_code=config.XROAD_ACCESS_GRANTS_SERVICE_SERVICE_CODE,
            service_service_version=config.XROAD_ACCESS_GRANTS_SERVICE_SERVICE_VERSION,
            # Body
            user_personal_id=user_id_no_country,
            user_country_code=country_iso_3
        )

    async def query_access_grants(self, request_body):
        logger.debug("query_access_grants")

        try:
            response = await self.http_client.post(
                config.XROAD_SECURITY_SERVER_URL,
                data=request_body,
                headers={"Content-Type": "text/xml"},
                timeout=config.XROAD_SECURITY_SERVER_DEFAULT_TIMEOUT,
            )
            response.raise_for_status()

        except:
            logger.exception("Failed to retrieve access grants")
            raise GraphQLError("IACS_REMOTE_ERROR")

        return response

    def parse_access_grants_response(self, response):
        logger.debug("parse_access_grants_response")

        # Parse and clean out namespaces
        root = objectify.fromstring(response.content)
        for element in root.xpath("descendant-or-self::*"):
            if element.prefix:
                element.tag = etree.QName(element).localname

        path = objectify.ObjectPath(".Body.esindatavateParingResponse.esindatavad")
        if not path.hasattr(root):
            raise GraphQLError("IACS_DATA_ERROR")

        try:
            path = objectify.ObjectPath(
                ".Body.esindatavateParingResponse.esindatavad.esindatav"
            )
            if path.hasattr(root):
                return path.find(root)
            else:
                return []
        except:
            raise GraphQLError("IACS_DATA_ERROR")

    async def get_access_grants(self, user_id):
        logger.debug("get_access_grants")
        
        request_body = self.make_access_grants_request_body(user_id)
        response = await self.query_access_grants(request_body)
        grants = self.parse_access_grants_response(response)

        return grants

    def make_application_fields_request_body(self, user_id, holding_id, iacs_year=None):
        return self.application_fields_request_template.format(
            xroad_user_id=user_id,
            xroad_id=uuid.uuid4(),
            # Client header
            client_xroad_instance=config.XROAD_FAST_CLIENT_XROAD_INSTANCE,
            client_member_class=config.XROAD_FAST_CLIENT_MEMBER_CLASS,
            client_member_code=config.XROAD_FAST_CLIENT_MEMBER_CODE,
            client_subsystem_code=config.XROAD_FAST_CLIENT_SUBSYSTEM_CODE,
            # Service header
            service_xroad_instance=config.XROAD_APPLICATION_FIELDS_SERVICE_XROAD_INSTANCE,
            service_member_class=config.XROAD_APPLICATION_FIELDS_SERVICE_MEMBER_CLASS,
            service_member_code=config.XROAD_APPLICATION_FIELDS_SERVICE_MEMBER_CODE,
            service_subsystem_code=config.XROAD_APPLICATION_FIELDS_SERVICE_SUBSYSTEM_CODE,
            service_service_code=config.XROAD_APPLICATION_FIELDS_SERVICE_SERVICE_CODE,
            service_service_version=config.XROAD_APPLICATION_FIELDS_SERVICE_SERVICE_VERSION,
            # Body
            pria_client=holding_id,
            iacs_year=iacs_year or "",
        )

    async def query_application_fields(self, request_body):
        try:
            response = await self.http_client.post(
                config.XROAD_SECURITY_SERVER_URL,
                data=request_body,
                headers={"Content-Type": "text/xml"},
                timeout=config.XROAD_SECURITY_SERVER_DEFAULT_TIMEOUT,
            )
            response.raise_for_status()

        except:
            logging.exception("Failed to retrieve parcels")
            raise GraphQLError("IACS_REMOTE_ERROR")

        return response

    def parse_application_fields_response(self, response):
        logger.debug("parse_application_fields_response")

        try:
            # Parse and clean out namespaces
            root = objectify.fromstring(response.content)
            for element in root.xpath("descendant-or-self::*"):
                if element.prefix:
                    element.tag = etree.QName(element).localname

            # print(objectify.dump(root))

            path = objectify.ObjectPath(
                ".Body.taotlusePolludFaSTResponse.taotlusePollud"
            )
            if not path.hasattr(root):
                raise GraphQLError("IACS_DATA_ERROR")

            # Extract the list of fields
            # esindatav = None [ObjectifiedElement]
            #   isikukood = '1234567890' [StringElement]
            #   juriidilineNimi = 'MILVI VIIR' [StringElement]
            #   ettevotlusvorm = 'FIE' [StringElement]
            #   registrikood = 11830012 [IntElement]
            #   kontaktid = 'xxx.yyy@pria.ee' [StringElement]
            #   kontaktid = 12345678 [IntElement]
            #   kontaktid = 12345678 [IntElement]
            path = objectify.ObjectPath(
                ".Body.taotlusePolludFaSTResponse.taotlusePollud.pold"
            )
            if path.hasattr(root):
                return path.find(root)
            else:
                return []
        except:
            logger.exception("Failed to parse response")
            raise GraphQLError("IACS_DATA_ERROR")

    async def get_application_fields(self, user_id, holding_id, iacs_year):
        request_body = self.make_application_fields_request_body(
            user_id, holding_id, iacs_year
        )
        response = await self.query_application_fields(request_body)
        fields = self.parse_application_fields_response(response)

        return fields


xroad_pria_client = XRoadPRIAClient()
