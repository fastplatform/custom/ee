import os
import sys

from pathlib import Path, PurePath
import logging

from pydantic import BaseSettings

from typing import Dict, Union


class Settings(BaseSettings):

    API_DIR: Path = PurePath(__file__).parent / "api"

    API_GATEWAY_EXTERNAL_URL: str
    API_GATEWAY_FASTPLATFORM_URL: str

    API_GATEWAY_SERVICE_KEY: str

    OPENTELEMETRY_EXPORTER_JAEGER_SERVICE_NAME: str = "iacs-pria"
    OPENTELEMETRY_EXPORTER_JAEGER_AGENT_HOSTNAME: str = "127.0.0.1"
    OPENTELEMETRY_EXPORTER_JAEGER_AGENT_PORT: int = 5775
    OPENTELEMETRY_SAMPLING_RATIO: float = 0.1

    # X-Road FaST Security Server configuration
    XROAD_SECURITY_SERVER_URL: str = (
        "https://x-road-security-server"
    )
    XROAD_SECURITY_SERVER_DEFAULT_TIMEOUT: int = 30
    XROAD_SECURITY_SERVER_PUBLIC_KEY_PATH: str = str(
        PurePath(__file__).parent / "certificates" / "security_server" / "cert.pem"
    )

    # FaST client headers
    XROAD_FAST_CLIENT_XROAD_INSTANCE: str = "ee-test"
    XROAD_FAST_CLIENT_MEMBER_CLASS: str = "GOV"
    XROAD_FAST_CLIENT_MEMBER_CODE: str = "70005967"
    XROAD_FAST_CLIENT_SUBSYSTEM_CODE: str = "fast"

    # Access grants service headers
    XROAD_ACCESS_GRANTS_SERVICE_X_ROAD_INSTANCE: str = "ee-test"
    XROAD_ACCESS_GRANTS_SERVICE_MEMBER_CLASS: str = "GOV"
    XROAD_ACCESS_GRANTS_SERVICE_MEMBER_CODE: str = "70005967"
    XROAD_ACCESS_GRANTS_SERVICE_SUBSYSTEM_CODE: str = "toetusteregister"
    XROAD_ACCESS_GRANTS_SERVICE_SERVICE_CODE: str = "esindatavateParing"
    XROAD_ACCESS_GRANTS_SERVICE_SERVICE_VERSION: str = "v1"

    # Parcels service headers
    XROAD_APPLICATION_FIELDS_SERVICE_XROAD_INSTANCE: str = "ee-test"
    XROAD_APPLICATION_FIELDS_SERVICE_MEMBER_CLASS: str = "GOV"
    XROAD_APPLICATION_FIELDS_SERVICE_MEMBER_CODE: str = "70005967"
    XROAD_APPLICATION_FIELDS_SERVICE_SUBSYSTEM_CODE: str = "toetusteregister"
    XROAD_APPLICATION_FIELDS_SERVICE_SERVICE_CODE: str = "taotlusePolludFaST"
    XROAD_APPLICATION_FIELDS_SERVICE_SERVICE_VERSION: str = "v1"

    # For the IACS consent form
    IACS_DISPLAY_NAME: str = "Põllumajanduse Registrite ja Informatsiooni Ameti"
    IACS_SHORT_NAME: str = "PRIA"
    IACS_WEBSITE_URL: str = "https://www.pria.ee/"

    # For generating Authority IDs like PRIA:1234567
    AUTHORITY_ID_PREFIX: str = "PRIA"

    LOG_COLORS = bool(os.getenv("LOG_COLORS", sys.stdout.isatty()))
    LOG_FORMAT = str(os.getenv("LOG_FORMAT", "uvicorn"))
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info")).upper()
    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "fastapi": {"propagate": True},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }

config = Settings()