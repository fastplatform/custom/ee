import os
import logging
import uvicorn

from fastapi import FastAPI

import graphene
from starlette_graphene3 import GraphQLApp

from app.db.graphql_clients import external, fastplatform
from app.api.lib.xroad_pria.xroad_pria import xroad_pria_client

from app.settings import config
from app.tracing import Tracing

from app.api.query import Query
from app.api.mutation import Mutation

# FastAPI
app = FastAPI()

# GraphQL root
app.add_route(
    "/graphql",
    GraphQLApp(
        schema=graphene.Schema(query=Query, mutation=Mutation, auto_camelcase=False)
    ),
)

# OpenTelemetry
Tracing.init(app)

# Log
logger = logging.getLogger(__name__)


@app.on_event("startup")
async def startup():
    logger.debug(config)
    await fastplatform.connect()
    await external.connect()
    xroad_pria_client.create_http_client()


@app.on_event("shutdown")
async def shutdown():
    await fastplatform.close()
    await external.close()
    await xroad_pria_client.close_http_client()


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=7011)