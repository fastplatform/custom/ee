# IACS/PRIA: Connector for PRIA IACS APIs

This service is a connector to access IACS data on the PRIA IACS APIs and serve these capabilities as GraphQL mutations

It exposes 4 mutations:

- SyncHoldings: request the holdings that are accessible to the user (in the PRIA IACS) and create/update the corresponding Holdings and UserRelatedParties in the FaST database
- SyncHolding: query the lines of the CAP submission of a given holding, and convert those to HoldingCampaign, Plots and Site objects in the FaST database.
- InsertDemoHolding: create a demo holding for the user (where the user is the only UserRelatedParty), based on a fixed description of site/geometry available in the `demo/holding.json` file of this repository.
- DuplicateHoldingCampaign: will copy the farm holding definition (site, plots, plant varieties) from one campaign to another

### Environment variables

#### API gateways
- `API_GATEWAY_EXTERNAL_URL`: URL to the external API Gateway (public data)
- `API_GATEWAY_FASTPLATFORM_URL`: URL to the fastplatform API Gateway (private data)
- `API_GATEWAY_SERVICE_KEY`: Service key to authenticate for the API gateways

#### Telemetry
- `OPENTELEMETRY_EXPORTER_JAEGER_SERVICE_NAME`: Service name for tracing (defaults to `"iacs-pria"`)
- `OPENTELEMETRY_EXPORTER_JAEGER_AGENT_HOSTNAME`: Hostname of the Jaeger agent (defaults to `"127.0.0.1"`)
- `OPENTELEMETRY_EXPORTER_JAEGER_AGENT_PORT`: Port of the Jaeger agent (defaults to `5775`)
- `OPENTELEMETRY_SAMPLING_RATIO`: Tracing sampling ratio (defaults to `0.1`)

#### Authority metadata
- `IACS_DISPLAY_NAME`: defaults to `"Põllumajanduse Registrite ja Informatsiooni Ameti"`
- `IACS_SHORT_NAME`: defaults to `"PRIA"`
- `IACS_WEBSITE_URL`: defaults to `"https://www.pria.ee/"`
#### X-Road / PRIA

##### Security server config
- `XROAD_SECURITY_SERVER_URL`: URL of the X-Road security server for FaST
- `XROAD_SECURITY_SERVER_DEFAULT_TIMEOUT`: timeout of requests to the security server (defauts to `30` seconds)
- `XROAD_SECURITY_SERVER_PUBLIC_KEY_PATH`: local path to the public key of the self-signed HTTPS certificate of the security server

##### FaST client headers
- `XROAD_FAST_CLIENT_XROAD_INSTANCE`: defaults to `"ee-test"`
- `XROAD_FAST_CLIENT_MEMBER_CLASS`: defaults to `"GOV"`
- `XROAD_FAST_CLIENT_MEMBER_CODE`: defaults to `"70005967"`
- `XROAD_FAST_CLIENT_SUBSYSTEM_CODE`: defaults to `"fast"`

##### Access grants service headers
- `XROAD_ACCESS_GRANTS_SERVICE_X_ROAD_INSTANCE`: defaults to `"ee-test"`
- `XROAD_ACCESS_GRANTS_SERVICE_MEMBER_CLASS`: defaults to `"GOV"`
- `XROAD_ACCESS_GRANTS_SERVICE_MEMBER_CODE`: defaults to `"70005967"`
- `XROAD_ACCESS_GRANTS_SERVICE_SUBSYSTEM_CODE`: defaults to `"toetusteregister"`
- `XROAD_ACCESS_GRANTS_SERVICE_SERVICE_CODE`: defaults to `"esindatavateParing"`
- `XROAD_ACCESS_GRANTS_SERVICE_SERVICE_VERSION`: defaults to `"v1"`

##### Parcels service headers
- `XROAD_APPLICATION_FIELDS_SERVICE_XROAD_INSTANCE`: defaults to `"ee-test"`
- `XROAD_APPLICATION_FIELDS_SERVICE_MEMBER_CLASS`: defaults to `"GOV"`
- `XROAD_APPLICATION_FIELDS_SERVICE_MEMBER_CODE`: defaults to `"70005967"`
- `XROAD_APPLICATION_FIELDS_SERVICE_SUBSYSTEM_CODE`: defaults to `"toetusteregister"`
- `XROAD_APPLICATION_FIELDS_SERVICE_SERVICE_CODE`: defaults to `"taotlusePolludFaST"`
- `XROAD_APPLICATION_FIELDS_SERVICE_SERVICE_VERSION`: defaults to `"v1"`

## High level description of the service

This connector is designed to map the return of the PRIA IACS APIs to FaST objects and create those objects through the FaST API gateways.

2 IACS APIs are exposed to FaST and map to 2 FaST mutations:
- the **Access Grants API** (*esindatavateParing*) --> `SyncHoldings`
- the **Application Fields API** (*taotlusePollud*) --> `SyncHolding`

The SyncHoldings mutation generates, in the FaST database, the creation of farms and grant access rights to the user to those farms. When called, this mutation will:
- check the identity of the user
- request the access grants of the user to the PRIA system, through the **Access Grants API**
- convert the received access grants to Holding objects and upsert those Holdings in FaST (ie it will insert the holdings that do not already exist and update the existong ones), and then convert those access grants to UserRelatedParty objects for the requesting user, hence making the user a member of those holdings
- return the list of holdings that have been upserted
- these holdings are now accessible to the user in the app

> Note: the SyncHoldings mutation will *not* remove existing access rights from FaST. This means that if User A was granted access to farm F in PRIA, then connected on FaST and pulled his access rights from PRIA (hence creating farm F in FaST and getting access to it in FaST), and later the access rights of User A on Farm F are revoked in PRIA, this will *not* revoke the access right of User A on farm F **in FaST**.

The SyncHolding mutation generates, for a given holding, the creation or update of a campaign based on the data returned by the **Application Fields API**. When called, this mutation will:
- check the identity of the user
- check that the user has edit rights to the holding (ie that the user is related party to the holding)
- request the fields of the CAP application of the user through the **Application Fields API**
- convert each received field to a Plot object:
  - reproject the field polygon to ETRS89
  - create a plot name based on the field identifiers
  - create a plot id based on the field identifiers
- create a unique Site object that contains the received plots, with the same name as the Holding
- create a HoldingCampaign object that contains this Site object
- return the list of plots that have been created

```plantuml
package "PRIA IACS APIs" as apis <<Rectangle>> {
    class AccessGrantsResponse {
        juriidilineNimi
        ettevotlusvorm
        registrikood
        eesnimi
        perekonnanimi
        kontaktid
    }
    class ApplicationFieldsResponse {
        polluId
        polluNr
        massiiviNr
        maaAlaNr
        pindala
        kultuuriKood
        kasMaheseeme
        kasHeinaseemnePold
        maakasutus
        geomeetria
        lisamiseKuupaev
        muutmiseKuupaev
    }

}

package "Context" as context <<Rectangle>> {
    class CurrentUser {
        username
    }
}

package "FaST Data Model" as fast_data_model <<Rectangle>> {
    class Holding {
        id
        authority_id
        name
    }
    class Campaign
    class UserRelatedParty {
        user : User
        role
    }
    class HoldingCampaign
    class Site {
        authority_id
        name
    }
    class Plot {
        authority_id
        name
        geometry
    }
    class Address {
        administrative_unit_level_1
        administrative_unit_level_2
        administrative_unit_level_3
        administrative_unit_level_4
    }
    class PlotPlantVariety {
        irrigation_method
        percentage
        plant_variety : PlantVariety
    }
    
    Holding "1" *-- "0..*" HoldingCampaign
    Campaign "1" *-- "0..*" HoldingCampaign
    HoldingCampaign "1" *-- "0..*" Site
    Site "1" *-- "0..*" Plot
    Plot "1" *-- "1" Address
    Plot "1" *-- "0..*" PlotPlantVariety
    Holding "1" *-- "0..*" UserRelatedParty
}

CurrentUser::username -[#blue]-> UserRelatedParty::user

AccessGrantsResponse::registrikood -[#blue]-> Holding::id : if company
AccessGrantsResponse::isikukood -[#blue]-> Holding::id : if person
AccessGrantsResponse::juriidilineNimi -[#blue]-> Holding::name : if company
AccessGrantsResponse::eesnimi -[#blue]-> Holding::name : if person
AccessGrantsResponse::perekonnanimi -[#blue]-> Holding::name : if person

ApplicationFieldsResponse::kultuuriKood -[#blue]-> PlotPlantVariety::plant_variety
ApplicationFieldsResponse::maakond -[#blue]-> Address::administrative_unit_level_2
ApplicationFieldsResponse::massiiviNr -[#blue]-> Plot::name
ApplicationFieldsResponse::polluNr -[#blue]-> Plot::name
ApplicationFieldsResponse::maaAlaNr -[#blue]-> Plot::name
ApplicationFieldsResponse::polluId -[#blue]-> Plot::authority_id
ApplicationFieldsResponse::geomeetria -[#blue]-> Plot::geometry

Holding::id -[#blue]-> Site::authority_id
Holding::name -[#blue]-> Site::name
```

## Setting up for Development

Create a virtualenv, activate it and install dependencies:
```bash
virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt
```

Start the GraphQL server on port 9999:
```bash
make start
```
The server is then ready to receive queries at http://localhost:7777/graphql, with live reload enabled.

### Bazel

Bazel targets can be used to run the service locally:

```bash
bazel run .
```
or
```bash
bazel run //services/meteorology/weather
```

## Sample 

Sample GraphQL queries are included in the `/tests` folder. You can also check the `Makefile` for examples on how these queries are called.