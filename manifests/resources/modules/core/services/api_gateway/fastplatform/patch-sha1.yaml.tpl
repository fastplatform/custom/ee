apiVersion: builtin
kind: PatchTransformer
metadata:
  name: api-gateway-fastplatform-metadata-sha1
patch: |-
  - op: add
    path: '/spec/template/spec/containers/0/env/-'
    value:
      name: HASURA_GRAPHQL_METADATA_SHASUM
      value: "INJECTED_AT_BUIL_TIME_BY_BAZEL"
target:
  group: serving.knative.dev
  kind: Service
  name: api-gateway-fastplatform
  version: v1